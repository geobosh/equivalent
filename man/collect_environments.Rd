\name{collect_environments}
\alias{collect_environments}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{Collect environments from an object}
\description{Collect environments from an object.}
\usage{
collect_environments(x, parents = FALSE, unique = TRUE)
}
\arguments{
  \item{x}{a list.}
  \item{parents}{
    if TRUE, return along with each environment its enclosing
    environment.
  }
  \item{unique}{
    if TRUE, drop duplicated elements from the result.
  }
}
\details{
  \code{collect_environments} gathers the environments in \code{x}. It
  examines the elements of \code{x} and, recursively, all list elements
  of \code{x}. If \code{parents} is TRUE it collects also the enclosing
  environments.

  \code{collect_environments} does not examine the contents of the
  environments if finds. The bulk of the work is done by \code{rapply}.
%%  ~~ If necessary, more details than the description above ~~
}
\value{
  a list of environments.
}
\author{Georgi N. Boshnakov}
\examples{
## todo:
}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ ~kwd1 }
